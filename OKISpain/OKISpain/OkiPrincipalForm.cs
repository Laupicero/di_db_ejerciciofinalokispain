﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Runtime.InteropServices;
using System.Drawing.Printing;


namespace OKISpain
{
    public partial class FormPrincipal : Form
    {
        private Clases.BaseDatosDAO DB_OKISpain;
        private Clases.FicherosTextoDAO OKI_FicherosTXT;
        private string currentDir;

        // Constructor
        public FormPrincipal()
        {
            InitializeComponent();

            this.currentDir = "C:\\XMLs";
            this.OKI_FicherosTXT = new Clases.FicherosTextoDAO(this.currentDir);

            pbOKIImg.SizeMode = PictureBoxSizeMode.StretchImage;  // Centramos la imagen del logo OKI en el pictureBox

            // Paso 'a' y 'b'
            realizarConexion();
            rellenarListBoxPedidos();
        }
        // Para poder mover nuestro formPersonalizado
        #region
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);
        private void FormPrincipal_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panelControlMenu_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panelNav_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        #endregion



        // --------------------------
        //  EVENTOS PRINCIPALES 
        // --------------------------
        //----------------
        //  BOTONES
        //----------------

        // Nos cierra la Ventana de la plicación, pero antes nos preguntarási la deseamos cerrar
        private void btnClose_Click(object sender, EventArgs e)
        {
            DialogResult confimacion = MessageBox.Show("¿Salir del programa, estás seguro?", "S A L I R", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information);

            if (confimacion == DialogResult.Yes)
                Application.Exit();
        }

        // Nos volverá a cargar nuestro ListBox de pedidos 
        // Por si el usuario ha insertado nuevos documentos xml en nuestra carpeta
        private void btnReload_Click(object sender, EventArgs e)
        {
            lbOrders.Items.Clear();
            rellenarListBoxPedidos();
        }


        //------------------------
        // Nos saldrá la opción de imprimir un pedido que seleccionemos
        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                PrintDocument doc1 = new PrintDocument();
                PrinterSettings ps = new PrinterSettings();
                doc1.PrinterSettings = ps;
                doc1.PrintPage += Imprimir;

                PrintPreviewDialog ppd = new PrintPreviewDialog { Document = doc1 };
                ((Form)ppd).WindowState = FormWindowState.Maximized;
                ppd.ShowDialog();

            }
            else
                MessageBox.Show("Seleccione primero un pedido para imprimir sus datos", " I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void Imprimir(object sender, PrintPageEventArgs e)
        {
            try
            {
                List<String> datosFicheroXML = Clases.UtilidadesXML.leerFicheroXMLCompleto(this.currentDir, lbOrders.SelectedItem.ToString());

                Font miFuente = new Font("Verdana", 12, FontStyle.Regular);
                int top = 15;
                int leftTitulo = 150;
                int leftDatos = 150;

                e.Graphics.DrawString("DATOS DEL FICHERO / PEDIDO", miFuente, Brushes.Blue, 200, top += 25);
                e.Graphics.DrawString("\n\n\n\n", miFuente, Brushes.Blue, leftTitulo, top += 25);

                for (int i = 0; i < datosFicheroXML.Count; i++)
                    e.Graphics.DrawString(datosFicheroXML[i], miFuente, Brushes.Black, leftDatos, top += 25);

            }
            catch (Exception ex)
            {
                MessageBox.Show("¡No se ha podido imprimir el fichero!\n" + ex.Message, "I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //------------------------


        // Paso c. Procesar XML
        // Nos dará la opción de procesar un pedido
        // 1- Comprobamos que haya uno seleccionado
        // 2- Comprobamos que no se haya procesado antes
        private void btnProcessOrder_Click(object sender, EventArgs e)
        {
            if (lbOrders.SelectedItem != null)
            {
                procesarPedidoSeleccionado(lbOrders.SelectedItem.ToString());

                MessageBox.Show("Se ha procesado correctamente su pedido y enviado los correos correspondientes", " I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rellenarListBoxPedidos();
            }
            else
            {
                procesarListboxPedidos();

                MessageBox.Show("Se han procesado todos sus pedidos y enviado los correos correspondientes", " I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
                rellenarListBoxPedidos();
            }
        }

        // --------------------------
        //  MÉTODOS
        // --------------------------



        // Para procesar un único pedido
        private void procesarPedido(Pedido pedido)
        {
            // Nos creamos un único pedido
            List<string> erroresPedidoList = new List<string>();

            try
            {
                // d. Comprobacion del stock de los articulos y realizaremos las acciones correspondientes sobre el pedido
                bool stockSuficiente = this.DB_OKISpain.comprobacionStockArticulos(ref pedido);

                //e. Si hay Stock Suficiente, lo reservará para el envío (tabla Existencias)
                if (stockSuficiente)
                {
                    bool resservaPedidosOK = this.DB_OKISpain.reservarArticulosParaElEnvio(pedido);

                    // Sino se ha podido reservar los artículos del pedido informará del error
                    if (!resservaPedidosOK)
                    {
                        // MessageBox.Show("No se han podido reservar correctamente la cantidad de los artículos para el pedido.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        erroresPedidoList.Add("No se han podido reservar correctamente la cantidad de los artículos para el pedido.");
                    }

                }
                else
                // f. Si no hay material/stock suficiente se crea una orden de pedido (tabla pedidos_compra)
                {
                    // Insertamos en la tabla 'pedidos_compra' (Simula que pedimos al almacén)
                    this.DB_OKISpain.inserccion_OrdenDePedido(pedido);
                    // Nos creamos un listado con los artículos fuera de stock
                    List<Articulo> articulosSinStock = this.DB_OKISpain.obtenerArticulosSinStock(pedido.ArticulosDelPedido);

                    //paso i. enviará una orden de pedido con todo el material que falta (un fichero pedidocompra_ddmmyy.txt)
                    // No creamos el fichero de texto
                    this.OKI_FicherosTXT.creacionFicheroOrdenPedido_PedidoCompra(ref pedido, articulosSinStock);


                    // paso i. Se enviará una orden de pedido con todo el material que falta(un fichero con todo elmaterial que falta 'pedidocompra_ddmmyy.txt')
                    bool opEnvioCorreoAlmacen = Clases.UtilidadesEnvioCorreo.operacionEnvioFicheroOrdenPedido_PedidoCompra(pedido.RutaFicheroPedidoCompra);

                    if (!opEnvioCorreoAlmacen)
                    {
                        erroresPedidoList.Add("No se ha podido enviar Correctamente el documento 'pedidocompra' al Almacen");
                        // MessageBox.Show("No se ha podido crear Correctamente el documento 'pedidocompra' para enviar al Almacen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }

                //paso g (suponiendo que es comun a 'e' y 'f')
                // Se añadirá/insertará los datos del pedido, a las tablas pedido_suministros_cabecera y pedidos_suministros_detalle 
                bool inserccionPedidoSuministrosCabecera = this.DB_OKISpain.inserccionTablaPedidoSuministrosCabecera(pedido);
                bool inserccionPedidoSuministrosDetalle = this.DB_OKISpain.inserccionTablaPedidoSuministrosDetalle(pedido);



                //Comprobamos si las insercciones se han realizado correctamente
                if (inserccionPedidoSuministrosCabecera && inserccionPedidoSuministrosDetalle)
                {
                    //  MessageBox.Show("Se han insertado correctamente en ambas tablas los datos del pedido.\n" +
                    //      "Con 'Num_pedido' en ambas '" + pedido.NumPedido_TicketID + "'.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    erroresPedidoList.Add("El pedido '" + pedido.NumPedido_TicketID + "' ya está registrado");

                    // MessageBox.Show("El pedido '" + pedido.NumPedido_TicketID + "' ya está registrado", "E R R O R",
                    //   MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //paso h. Enviar archivo de texto con el material adjunto y su número de pedido (fichero 'workcenter_ddmmyy.txt)
                OKI_FicherosTXT.crearFicheroOrdenDePedidoParaTaller(ref pedido);


                bool opEnvioCorreoTaller = Clases.UtilidadesEnvioCorreo.operacionEnvioFicheroOrdenPedido_Talleres(pedido.RutaFicheroWorkCenter, pedido.RutaFicheroPedidoCompra);

                if (!opEnvioCorreoTaller)
                {
                    erroresPedidoList.Add("No se han podido enviar los datos del pedido '" + pedido.NumPedido_TicketID + "', ya procesado, a su taller; informando del envío.");

                    //  MessageBox.Show("No se han podido enviar los datos del pedido '" + pedido.NumPedido_TicketID + "', ya procesado a su taller, informando del envío.", "E R R O R",
                    //    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            // Comprobamos todos lo errores posibles e informaremos de ello, a través de un messageBox
            // k. Si hubiera algún XML que diera fallo se moverá a una subcarpeta llamada “errores” 
            catch (Exception ex)
            {
                MessageBox.Show("¡ERROR!\n" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            finally
            {
                // l.Por último se enviará un archivo de texto llamado Proceso_ddmmyy.txt a una dirección determinada, con la información de los pedidos enviados y los pedidos con errores
                OKI_FicherosTXT.crearFicheroOrdenDeProceso(ref pedido, erroresPedidoList);
                bool opEnvioCorreoProcesos = Clases.UtilidadesEnvioCorreo.operacionEnvioFicheroProceso(pedido.RutaFicheroProceso);
                OKI_FicherosTXT.borrarFicheros(pedido);
            }
        }

        //Para procesar un pedido seleccionado del listbox
        private void procesarPedidoSeleccionado(string nombreFichero)
        {
            try
            {
                if (!comprobacionPedidoProcesado(nombreFichero))
                {
                    Pedido pedido = new Pedido();
                    // c. Leemos los datos del XML y nos devolverá un listado de los artículos del Pedido a procesar
                    // Además, de datos sobre el pedido que necesitaremos
                    // Y al final, Añadimos a nuestro pedido su listado de artículos (incluímos el paso 'k')
                    List<Articulo> articulos = Clases.UtilidadesXML.leerDatosPedidoXML(nombreFichero, this.currentDir);
                    Clases.UtilidadesXML.obtenerDatosPedido(ref pedido, nombreFichero, this.currentDir);
                    pedido.ArticulosDelPedido = articulos;

                    if (pedido.NumPedido_TicketID != null && pedido.ArticulosDelPedido != null && pedido.ArticulosDelPedido.Count > 0 && pedido.WorkCenter != null)
                    {
                        this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaImportados(nombreFichero);
                        procesarPedido(pedido);

                    }
                    else
                    {
                        MessageBox.Show("No se puede leer el fichero '" + nombreFichero + "' por falta de datos esenciales", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
                    }
                }
                else
                {
                    MessageBox.Show("¡Este pedido ya ha sido procesado con anterioridad, por favor asegúrese primero!", "I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("¡ERROR!\n" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
            }
        }


        // Para procesar Todos los pedidos del listbox
        private void procesarListboxPedidos()
        {

            //Parseo todos los ficheros a modelo Pedido
            List<Pedido> listaPedidosCompleta = new List<Pedido>();
            for (int i = 0; i < lbOrders.Items.Count; i++)
            {
                string nombreFichero = lbOrders.Items[i].ToString();
                // Comprobamos que no se haya procesado con anterioridad

                try
                {
                    if (!comprobacionPedidoProcesado(nombreFichero))
                    {
                        Pedido pedido = new Pedido();
                        // c. Leemos los datos del XML y nos devolverá un listado de los artículos del Pedido a procesar
                        // Además, de datos sobre el pedido que necesitaremos
                        // Y al final, Añadimos a nuestro pedido su listado de artículos (incluímos el paso 'k')
                        List<Articulo> articulos = Clases.UtilidadesXML.leerDatosPedidoXML(nombreFichero, this.currentDir);
                        Clases.UtilidadesXML.obtenerDatosPedido(ref pedido, nombreFichero, this.currentDir);
                        pedido.ArticulosDelPedido = articulos;

                        if (pedido.NumPedido_TicketID != null && pedido.ArticulosDelPedido != null && pedido.ArticulosDelPedido.Count > 0 && pedido.WorkCenter != null)
                        {
                            this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaImportados(nombreFichero);
                            listaPedidosCompleta.Add(pedido);
                        }
                        else
                        {
                            MessageBox.Show("No se puede leer el fichero '" + nombreFichero + "' por falta de datos esenciales", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
                        }
                    }
                    else
                    {
                        MessageBox.Show("¡Este pedido ya ha sido procesado con anterioridad, por favor asegúrese primero!", "I N F O R M A C I Ó N", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("¡ERROR!\n" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.OKI_FicherosTXT.enviarXMLProcesadoSubCarpetaErrores(nombreFichero);
                }
            }

            //Fusionar
            List<Pedido> pedidosFusionados = fusionarPedidos(listaPedidosCompleta);

            //Por cada pedido unico, hacemos toda la logica
            foreach (Pedido pedido in pedidosFusionados)
            {
                procesarPedido(pedido);
            }
        }




        // Nos limpia el listado (list Box) de nuestros pedidos/ ficheros xml
        private void btnCleanLbOrder_Click(object sender, EventArgs e)
        {
            lbOrders.Items.Clear();
        }
        // --------------------------
        //  MÉTODOS AUXILIARES
        // --------------------------

        // Método para realizar nuestra conexión a nuestra DB
        private void realizarConexion()
        {
            this.DB_OKISpain = new Clases.BaseDatosDAO(this.currentDir);
        }

        // Nos rellena nuestro listBox con nuestros ficheros XML
        // Hemos usado una ruta relativa que sera dentro del mismo programa
        private void rellenarListBoxPedidos()
        {
            lbOrders.Items.Clear();
            try
            {
                List<string> ficherosXML = this.OKI_FicherosTXT.obtenerNumeroFicherosXML();

                foreach (string nombreFichero in ficherosXML)
                    lbOrders.Items.Add(nombreFichero);
            }
            catch (Exception ex)
            {
                MessageBox.Show("¡ERROR!\n" + ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        // Nos asegúramos con este método, de que nuestro pedido no sea repetido. Devolverá:
        // true - Si esta repetido
        // false, sino está repetido
        // Lo comrpobaremos haciendo la consulta en la tabla de 'importados'
        private bool comprobacionPedidoProcesado(String nombrePedido)
        {
            bool pedidoProcesado = true;
            int result = this.DB_OKISpain.consultarPedidosProcesados(nombrePedido);

            if (result == 0)
                MessageBox.Show("El pedido ya está registrado en nuestra Base de datos.\n" +
                    "Ya se procesó con anterioridad", "E R R O R", MessageBoxButtons.OK, MessageBoxIcon.Information);

            else if (result == -1)
                MessageBox.Show("Ocurrío un error inesperado:\n", "E R R O R", MessageBoxButtons.OK, MessageBoxIcon.Information);

            else
                pedidoProcesado = false;

            return pedidoProcesado;
        }



        // Al encontrar un ficheroXML/pedido con el mismo 'Ticket-ID'
        // Se fusionarán en un mismo pedido
        private List<Pedido> fusionarPedidos(List<Pedido> pedidos)
        {
            List<Pedido> pedidosFusionados = new List<Pedido>();
            // Buscamos el peiddo que coincida
            foreach (Pedido ped in pedidos)
            {

                int existePedidoIndex = pedidosFusionados.FindIndex(pedidoIndex => pedidoIndex.NumPedido_TicketID == ped.NumPedido_TicketID); 
                if (existePedidoIndex > -1)
                {
                    pedidosFusionados[existePedidoIndex].ArticulosDelPedido.AddRange(ped.ArticulosDelPedido);
                }
                else
                {
                    pedidosFusionados.Add(ped);
                }

            }

            return pedidosFusionados;
        }


    }
}
