﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OKISpain.Clases
{
    class FicherosTextoDAO
    {
        // Variable para saber la ruta de los ficheros
        private string direccionActual;

        // --------------------------------
        //  Constructor
        // --------------------------------
        public FicherosTextoDAO(string currentDir)
        {
            this.direccionActual = currentDir;
        }


        //--------------------------------------------------------------------------------------
        //      MÉTODOS ESTÁTICOS PRINCIPALES (Servirán como una especie de controladores)
        // Son los que se comunican directamente con las acciones de nuestro 'Formulario Principal' - 'OkiPrincipalForm.cs'
        //--------------------------------------------------------------------------------------

        //---------------
        // Obtendremos  un listado de todos los ficheros 'xml' (comprobando su extensión)
        // Que se encuentren en el determinado directorio indicado por nosotros
        internal List<string> obtenerNumeroFicherosXML()
        {
            List<string> ficherosXML = new List<string>();
            DirectoryInfo dInfo = new DirectoryInfo(this.direccionActual);

            foreach (FileInfo file in dInfo.GetFiles())
            {
                if (file.Extension == ".xml")
                    ficherosXML.Add(file.Name);
            }
            return ficherosXML;
        }
        //---------------


        //---------------
        // Nos creará el fichero 'pedidocompra__ddmmyy.txt ' en la ruta correspondiente
        internal void creacionFicheroOrdenPedido_PedidoCompra(ref Pedido pedido, List<Articulo> articulosSinStock)
        {

            //Para el nombre del fichero coemos la fecha actual y le daremos el formato establecido
            DateTime fechaActual = DateTime.Now;
            pedido.RutaFicheroPedidoCompra = this.direccionActual + "\\ficherosTexto\\pedidocompra__" + fechaActual.ToString("ddMMyy") + ".txt";

            StreamWriter sw = File.CreateText(pedido.RutaFicheroPedidoCompra);
            sw.WriteLine(datosPedidoPedidoComprAlmacen(pedido, articulosSinStock));
            sw.Close();
        }
        //---------------



        //---------------
        // Nos creará el fichero 'workcenter__ddmmyy.txt ' en la ruta correspondiente
        internal void crearFicheroOrdenDePedidoParaTaller(ref Pedido pedido)
        {
            //Para el nombre del fichero coemos la fecha actual y le daremos el formato establecido
            DateTime fechaActual = DateTime.Now;
            pedido.RutaFicheroWorkCenter += this.direccionActual + "\\ficherosTexto\\workcenter__" + fechaActual.ToString("ddMMyy") + ".txt";

            StreamWriter sw = File.CreateText(pedido.RutaFicheroWorkCenter);
            sw.WriteLine(datosPedidoPedidoCompraTaller(pedido));
            sw.Close();
        }
        //---------------



        //---------------
        // Nos creará el fichero 'Procceso_ddmmyy.txt ' en la ruta correspondiente
        internal void crearFicheroOrdenDeProceso(ref Pedido pedido, List<string> errores)
        {
            //Para el nombre del fichero coemos la fecha actual y le daremos el formato establecido
            DateTime fechaActual = DateTime.Now;
            pedido.RutaFicheroProceso += this.direccionActual + "\\ficherosTexto\\Proceso__" + fechaActual.ToString("ddMMyy") + ".txt";

            StreamWriter sw = File.CreateText(pedido.RutaFicheroProceso);
            if(pedido.NumPedido_TicketID != null && pedido.ArticulosDelPedido != null)
                sw.WriteLine(datosPedidoPedidoCompraTaller(pedido));

            foreach (string error in errores)
            {
                sw.WriteLine("\n" + error);
            }
            
            sw.Close();
        }
        //---------------



        //---------------
        // Borrado de ficheros
        internal void borrarFicheros(Pedido pedido)
        {
            if (pedido.RutaFicheroPedidoCompra != null)
                File.Delete(pedido.RutaFicheroPedidoCompra);

            if (pedido.RutaFicheroProceso != null)
                File.Delete(pedido.RutaFicheroProceso);

            if (pedido.RutaFicheroWorkCenter != null)
                File.Delete(pedido.RutaFicheroWorkCenter);
        }
        //---------------



        //---------------
        // Nos cambia el fichero xml del pedido a la subcarpeta de  'importados'
        internal void enviarXMLProcesadoSubCarpetaImportados(string nombreFichero)
        {
            try
            {
                string ruta = this.direccionActual + "\\" + nombreFichero;
                string nuevaRuta = this.direccionActual + "\\importado\\" + nombreFichero;

                if (!File.Exists(ruta))
                    File.Create(ruta);

                if (File.Exists(nuevaRuta))
                    File.Delete(nuevaRuta);

                File.Move(ruta, nuevaRuta);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }            
        }
        //---------------


        //---------------
        // Nos cambia el fichero xml del pedido a la subcarpeta de  'errores'
        internal void enviarXMLProcesadoSubCarpetaErrores(string nombreFichero)
        {
            try
            {
                string ruta = this.direccionActual + "\\" + nombreFichero;
                string nuevaRuta = this.direccionActual + "\\errores\\" + nombreFichero;

                if (!File.Exists(ruta))
                    File.Create(ruta);

                if (File.Exists(nuevaRuta))
                    File.Delete(nuevaRuta);

                File.Move(ruta, nuevaRuta);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        //---------------


        //--------------------------------------------------
        //          MÉTODOS AUXILIARES DE APOYO
        //--------------------------------------------------

        //---------------
        // Nos devuelve una cadena de 'string' bien formateada
        // para poder incluirla en nuestro fichero 'workcentertxt'
        private string datosPedidoPedidoCompraTaller(Pedido pedido)
        {
            String articulosPedido = "";

            // Por Cada Artículo
            foreach(Articulo art in pedido.ArticulosDelPedido)
            {
                articulosPedido += "\n\t [" + art.CodPedido + "] - " + art.Descrip;
            }

            return String.Format("PEDIDO PROCESADO PARA EL CLIENTE {0} \t\t- WORKCENTER[{1}]  \t\t{2}\n\n\n" +

                "\nDIRECCIÓN: {3}" +
                "\nLOCALIDAD: {4}" +
                "\nCP: {5}" +
                "\n\tDeSCRIPCIÓN [ENG]: {6}" +
                "\n\tPEDIDOS: {7}", 

                pedido.Nomb_Cliente_Submitter,
                pedido.WorkCenter,
                pedido.Codigo_Warranty_Articlenumber,
                pedido.Dir_Cliente_Loc_Street,
                pedido.PoblacionCliente_LocCity,
                pedido.CP_Cliente_Loc_Post_code1,
                pedido.Descrip_Warranty_ArticleDescription,
                articulosPedido);
        }
        //---------------


        //---------------
        // Nos devuelve una cadena de 'string' bien formateada
        // para poder incluirla en nuestro fichero 'pedidocompratxt'
        private static string datosPedidoPedidoComprAlmacen(Pedido pedido, List<Articulo> articulosSinStock)
        {
            String articulosPedido = "";

            // Por Cada Artículo
            foreach (Articulo art in articulosSinStock)
            {
                articulosPedido += "\n\t [" + art.CodPedido + "] - " + art.Descrip;
            }

            return String.Format("PEDIDO PROCESADO PARA ALACENES OKISpain\n\n\n" +

                "\nDIRECCIÓN: AVDA Empresarial Saucedo S/N" +
                "\nLOCALIDAD: Pinto - MADRID" +
                "\nCP: 30450" +
                "\n\tNºPEdido: FC34325452" +
                "\n\tLISTADO MATERIAL: {0}",

                articulosPedido);
        }

        
    }
}
