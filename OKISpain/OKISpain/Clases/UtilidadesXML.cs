﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OKISpain.Clases
{
    class UtilidadesXML
    {

        //--------------------------------------------------------------------------------------
        //      MÉTODOS ESTÁTICOS PRINCIPALES (Servirán como una especie de controladores)
        // Son los que se comunican directamente con las acciones de nuestro 'Formulario Principal' - 'OkiPrincipalForm.cs'
        //--------------------------------------------------------------------------------------

        //Nos leerá el fichero XML Completo y nos lo devolverá en forma de listado
        internal static List<string> leerFicheroXMLCompleto(string directorioActual, string nombreFichero)
        {
            List<string> datos = new List<string>();
            string rutaFicheroXML = directorioActual + "\\" + nombreFichero;

            XmlReader reader = XmlReader.Create(rutaFicheroXML);

            try
            {
                while (reader.Read())
                    datos.Add("\n" + reader.LocalName + "--" + reader.Value);

                reader.Close();
                return datos;
            }
            catch (Exception ex)
            {
                reader.Close();
                return null;
            }
        }

        // nos leerá el fichero XML
        // y obtendremos todos los Datos del pedido 'ArticleNumber'
        // Nos dará un FileNotFoundException por nsi durante el proceso, el usuario
        // eliminase o moviese el fichero
        internal static List<Articulo> leerDatosPedidoXML(string nombrefichero, string directorioActual)
        {            
            // Obtenemos algunos datos de nuestro articulo/articulos (por si hay más de uno)
            // Y guardamos esos datos en una lista dinámica
             List<string> numArticulos = LeerNodosFicheroXML("ArticleNumber", nombrefichero, directorioActual);
             List<string> descrip = LeerNodosFicheroXML("ArticleDescription", nombrefichero, directorioActual);

             // Creamos el objeto/objetos 'Articulo'
             return Clases.UtilidadesPojo.generarListadoArticulos(numArticulos, descrip);
        }



        // Obtenemos los datos de nuestro pedido, que nos hará falta para procesar sus datos y hacer las insercciones correspondientes
        internal static void obtenerDatosPedido(ref Pedido pedido, string nombrefichero, string directorioActual)
        {
            pedido.NumPedido_TicketID = LeerNodoFicheroXML("TicketID", nombrefichero, directorioActual);
            pedido.Codigo_Warranty_Articlenumber = LeerNodoFicheroXML("Warranty_Articlenumber", nombrefichero, directorioActual);
            pedido.Descrip_Warranty_ArticleDescription = LeerNodoFicheroXML("Warranty_ArticleDescription", nombrefichero, directorioActual);
            pedido.Serie_Warranty_Serialnumber = LeerNodoFicheroXML("Warranty_Serialnumber", nombrefichero, directorioActual);
            pedido.WorkCenter = LeerNodoFicheroXML("WorkC_Work_Center", nombrefichero, directorioActual);
            pedido.CP_Cliente_Loc_Post_code1 = LeerNodoFicheroXML("Loc_Post_code", nombrefichero, directorioActual);
            pedido.PoblacionCliente_LocCity = LeerNodoFicheroXML("Loc_City", nombrefichero, directorioActual);
            pedido.Nomb_Cliente_Submitter = LeerNodoFicheroXML("Submitter", nombrefichero, directorioActual);
            string fecha = LeerNodoFicheroXML("XML_Date", nombrefichero, directorioActual);
            if(fecha != "")
            {
                pedido.Fecha_XML_Date = Convert.ToDateTime(fecha);
            }
            
            pedido.Dir_Cliente_Loc_Street = LeerNodoFicheroXML("Loc_Street", nombrefichero, directorioActual);
        }


        //--------------------------------------------------
        //          MÉTODOS AUXILIARES DE APOYO
        //--------------------------------------------------

        // Nos devolverá una Lista de 'string' con todos los resultados del nodo que haya encontrado
        private static List<string> LeerNodosFicheroXML(string nombreNodo, string nombreFichero, string directorioActual)
        {
            List<string> result = new List<string>();
            string rutaFicheroXML = directorioActual + "\\" + nombreFichero;

            XmlReader reader = XmlReader.Create(rutaFicheroXML);

            try
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement() && reader.Name.ToString() == nombreNodo)
                        result.Add(reader.ReadElementContentAsString());
                }
                reader.Close();
                return result;
            } catch(Exception ex)
            {
                reader.Close();
                return result;
            }
            
        }

       


        // Nos devolverá un 'string' con el resultado del nodo que haya encontrado
        private static string LeerNodoFicheroXML(string nombreNodo, string nombreFichero, string directorioActual)
        {
            string result = "";
            string rutaFicheroXML = directorioActual + "\\" + nombreFichero;

            XmlReader reader = XmlReader.Create(rutaFicheroXML);
            try
            {
                while (reader.Read())
                {
                    if (reader.IsStartElement() && reader.Name.ToString() == nombreNodo)
                        result = reader.ReadElementContentAsString();
                }
                reader.Close();
                return result;
            } catch(Exception ex)
            {
                reader.Close();
                return result;
            }
            
        }
    }
}
