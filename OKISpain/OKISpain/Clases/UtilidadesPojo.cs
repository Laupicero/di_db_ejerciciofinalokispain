﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OKISpain.Clases
{
    class UtilidadesPojo
    {

        //--------------------------------------------------------------------------------------
        //      MÉTODOS ESTÁTICOS PRINCIPALES (Servirán como una especie de controladores)
        // Son los que se comunican directamente con las acciones de nuestro 'Formulario Principal' - 'OkiPrincipalForm.cs'
        //--------------------------------------------------------------------------------------

        //--------------
        // Nos generará un listado de los 'Articulo' de nuestros pedidos
        public static List<Articulo> generarListadoArticulos(List<string> numArticulos, List<string> descrip)
        {
            List<Articulo> articulos = new List<Articulo>();

            // Comprobamos si son del mismo tamaño para poder asegurarnos que no se cuela ninguna descripción que no es la correspondiente del artículo
            if (numArticulos.Count == descrip.Count)
                articulos = generarInstanciasArticulos(numArticulos, descrip);
            else
                articulos = generarInstanciasArticulos(numArticulos);

            return articulos;
        }
        //--------------



        //--------------------------------------------------
        //          MÉTODOS AUXILIARES DE APOYO
        //--------------------------------------------------


        //--------------
        // Nos generará un listado con las instancias 'Articulo'
        // Tendrá un parámtero opcional que será detalles por si se encontrase algún error de coincidencia al leer el XML
        public static List<Articulo> generarInstanciasArticulos(List<string> numArticulos, List<string> descrip = null)
        {
            List<Articulo> articulos = new List<Articulo>();
            // Si la lista de descripciones coincide con la del número de los artículos
            if (descrip != null)
            {
                for (int i = 0; i < numArticulos.Count; i++)
                {
                    articulos.Add(new Articulo(
                        numArticulos[i],
                        descrip[i],
                        "OKI",
                        "NULL"));
                }
            }
            // Si la lista de descripciones por defecto es null/ no se la hemos pasado
            else
            {
                for (int i = 0; i < numArticulos.Count; i++)
                {
                    articulos.Add(new Articulo(
                        numArticulos[i],
                        " ",
                        "OKI",
                        "NULL"));
                }
            }
            return articulos;
        }
        //--------------
    }
}
