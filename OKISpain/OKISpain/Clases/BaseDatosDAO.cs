﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace OKISpain.Clases
{
    class BaseDatosDAO
    {
        private SqlConnection conexion;
        private string currentDir;

        //constructor
        public BaseDatosDAO(string dir)
        {
            this.currentDir = dir;                         // Establecemos el directorio por defecto
            Conectar_a_DB();
        }
        //--------------------------------------------------------------------------------------
        //      MÉTODOS ESTÁTICOS PRINCIPALES (Servirán como una especie de controladores)
        // Son los que se comunican directamente con las acciones de nuestro 'Formulario Principal' - 'OkiPrincipalForm.cs'
        //--------------------------------------------------------------------------------------

        //------------------
        // Nos conectará a la DB de 'OKI Spain'
        internal void Conectar_a_DB()
        {
            //Obtiene el 'string' de la conexión a través del 'app.config'
            string cadenaDeConexion =  ConfigurationManager.ConnectionStrings["conLaura"].ConnectionString;
            conexion = new SqlConnection(cadenaDeConexion);
            conexion.Open();
        }
        // Fin Conectar_a_DB


        //------------------
        // Nos devolverá un entero que determinará el resultado de la búsqueda
        // 1 -> El pedido no existe
        // 0 -> El pedido existe
        //-1 -> Ocurrío un error al realizar la consulta
        internal int consultarPedidosProcesados(string pedidoAComprobar)
        {
            try
            {
                int existePedido = 1;

                SqlCommand cmd = new SqlCommand("SELECT * FROM importadosOki", conexion);
                SqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                    if (dataReader["fichero"].ToString() == pedidoAComprobar)
                        existePedido = 0;

                dataReader.Close();
                return existePedido;

            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        // Fin consultarPedidosProcesados


        //------------------
        //  Nos comprobará que existe stock suficiente por cada artículo
        //  y lo reservará para el envío 
        internal bool comprobacionStockArticulos(ref Pedido pedido)
        {
            bool stockSuficiente = true;

            // Comprobamos por cada articulo de nuestro pedido que hay 'stock' suficiente
            foreach (Articulo art in pedido.ArticulosDelPedido)
            {
                SqlCommand cmd = new SqlCommand("select * from Existencias where codigo = '" +art.CodPedido+ "'", conexion);
                SqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    //Si hay stock
                    if (Convert.ToInt32(dataReader["cant_existente2"]) > 0)
                        stockSuficiente = true;
                    // Si no hay stock
                    else
                        stockSuficiente = false;
                }
                dataReader.Close();                
            }
            return stockSuficiente;
        }
        // Fin comprobacionStockArticulos


        //---------------
        // Se crea un inserccion en la tabla 'pedidos_compra'
        // Esto pasa cuando no hay stock/Cantidad suficiente del artículo
        public int inserccion_OrdenDePedido(Pedido ped)
        {
            try
            {
                String query = "insert into pedidos_compra(Num_pedido, Codigo, Descripcion, Cantidad)" +
                    "Values(@NumPedido, @Codigo, @Descrip, @Cant)";

                SqlCommand command = new SqlCommand(query, conexion);

                command.Parameters.AddWithValue("@NumPedido", ped.NumPedido_TicketID);
                command.Parameters.AddWithValue("@Codigo", ped.Codigo_Warranty_Articlenumber);
                command.Parameters.AddWithValue("@Descrip", ped.Descrip_Warranty_ArticleDescription);
                command.Parameters.AddWithValue("@Cant", 1);

                command.ExecuteNonQuery();
                return 1;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }


        // Nos devuelve el listado de los artículos que no tienen Stock
        internal List<Articulo> obtenerArticulosSinStock(List<Articulo> articulosDelPedido)
        {
            List<Articulo> articulos = new List<Articulo>();

            // Comprobamos por cada articulo de nuestro pedido que hay 'stock' suficiente
            foreach (Articulo art in articulosDelPedido)
            {
                SqlCommand cmd = new SqlCommand("select * from Existencias where codigo = '" + art.CodPedido + "'", conexion);
                SqlDataReader dataReader = cmd.ExecuteReader();

                while (dataReader.Read())
                {
                    //Si no hay stock, añadimos a nuestro listado
                    if (Convert.ToInt32(dataReader["cant_existente2"]) <= 0)
                        articulos.Add(art);
                }
                dataReader.Close();
            }
            return articulos;
        }
        //---------------





        //--------------------------------------------------
        //          MÉTODOS AUXILIARES DE APOYO
        //--------------------------------------------------


        

       


        //---------------
        // Insertamos los datos del pedido en la tabla 'pedidos_suministros_cabecera'
        // Nos devolverá un 'bool' en función de si ha ido bien o no el proceso
        internal bool inserccionTablaPedidoSuministrosCabecera(Pedido ped)
        {
            try
            {
                String query = "insert into pedidos_suministros_cabecera(Num_pedido, DOA, Fecha, serie, " +
                    "NOM_CLIENTE, DIR_CLIENTE, CP_CLIENTE, POBLACION_CLIENTE, FechaAveria, WORKC, fechamodificacion)" +
                    "values(@NumPedido, @DOA, @Fecha, @serie, @NOM_CLIENTE, @DIR_CLIENTE, @CP_CLIENTE, @POBLACION_CLIENTE, @FechaAveria, @WORKC, @fechamodificacion)";

                SqlCommand command = new SqlCommand(query, conexion);

                command.Parameters.AddWithValue("@NumPedido", ped.NumPedido_TicketID);
                command.Parameters.AddWithValue("@DOA", "");
                command.Parameters.AddWithValue("@Fecha", ped.Fecha_XML_Date);
                command.Parameters.AddWithValue("@serie", ped.Serie_Warranty_Serialnumber);
                command.Parameters.AddWithValue("@NOM_CLIENTE", ped.Nomb_Cliente_Submitter);
                command.Parameters.AddWithValue("@DIR_CLIENTE", ped.Dir_Cliente_Loc_Street);
                command.Parameters.AddWithValue("@CP_CLIENTE", ped.CP_Cliente_Loc_Post_code1);
                command.Parameters.AddWithValue("@POBLACION_CLIENTE", ped.PoblacionCliente_LocCity);
                command.Parameters.AddWithValue("@FechaAveria", ped.Fecha_XML_Date);
                command.Parameters.AddWithValue("@WORKC", ped.WorkCenter);
                command.Parameters.AddWithValue("@fechamodificacion", ped.Fecha_XML_Date);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        //---------------



        //---------------
        // Insertamos los datos del pedido en la tabla 'pedidos_suministros_detalle'
        // Nos devolverá un 'bool' en función de si ha ido bien o no el proceso
        internal bool inserccionTablaPedidoSuministrosDetalle(Pedido ped)
        {
            try
            {
                String query = "insert into pedidos_suministros_detalle(Num_pedido, Codigo, Descripcion, Cantidad, cant_pedida, fechaEntrada, fechamodificacion)" +
                    "values(@NumPedido, @Codigo, @Descripcion, 1, 1, @fechaEntrada, @fechamodificacion)";

                SqlCommand command = new SqlCommand(query, conexion);

                command.Parameters.AddWithValue("@NumPedido", ped.NumPedido_TicketID);
                command.Parameters.AddWithValue("@Codigo", 45698877);
                command.Parameters.AddWithValue("@Descripcion", ped.Descrip_Warranty_ArticleDescription);
                command.Parameters.AddWithValue("@fechaEntrada", ped.Fecha_XML_Date);
                command.Parameters.AddWithValue("@fechamodificacion", ped.Fecha_XML_Date);

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        //---------------


        //---------------
        // Nos reservará la cantidad de los articulos del pedido en la tabla Existencias
        // Por cada Articulo, se aumentará la cantidad reservada en '1' y se restará la cant_existente, excepto si es '0'
        internal bool reservarArticulosParaElEnvio(Pedido pedido)
        {
            try
            {
                foreach(Articulo art in pedido.ArticulosDelPedido)
                {

                    // Aumentamos las cantidades reservadas
                    string query1 = "UPDATE Existencias SET cant_reservada += @cant1, cant_reservada2 += @cant1 Where codigo = @CodPedido";
                    SqlCommand command1 = new SqlCommand(query1, this.conexion);

                    command1.Parameters.AddWithValue("@cant1", 1);
                    command1.Parameters.AddWithValue("@CodPedido", art.CodPedido);

                    command1.ExecuteNonQuery();

                    // Restamos de las cantidades existentes salvo si es '0'
                    string query2 = "UPDATE Existencias SET cant_existente2 -= @cant1 Where codigo = @CodPedido AND cant_existente2 >= @cant1";
                    SqlCommand command2 = new SqlCommand(query2, this.conexion);

                    command2.Parameters.AddWithValue("@cant1", 1);
                    command2.Parameters.AddWithValue("@CodPedido", art.CodPedido);

                    command2.ExecuteNonQuery();
                }            
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
        //---------------

    }
}
