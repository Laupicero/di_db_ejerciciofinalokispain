﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace OKISpain.Clases
{
    class UtilidadesEnvioCorreo
    {

        //--------------------------------------------------------------------------------------
        //      MÉTODOS ESTÁTICOS PRINCIPALES (Servirán como una especie de controladores)
        // Son los que se comunican directamente con las acciones de nuestro 'Formulario Principal' - 'OkiPrincipalForm.cs'
        //--------------------------------------------------------------------------------------

        //Nos realiza el envío del correo a los Almacenes
        // Para solicitar los materiales/artículos que nos faltan de el pedido correspondiente
        internal static bool operacionEnvioFicheroOrdenPedido_PedidoCompra(string rutaFichero)
        {
            string remitente = "alumnos2damAlmunia@gmail.com";
            string miclave = "2dam2dam ";
            string destinatario = "llucbue379@iesalmunia.com";
            string asunto = "¡URGE! Pedido del material/Artículos sin Stock";
            string mensaje = "<h2>Desde Almacenes OKISpain</h2>\n" +
                "<p>Les informamos de los artículos que necesitamos y de los cuáles nos encontramos sin existencias</p>" +
                "<hr/>" +
                "<p>Aquí les dejamos todos los datos y la documentación pertinente</p>" +
                "<p>Les rogamos que los envien cuanto antes</p>" +
                "\n<br/>" +
                "<p>Pasen un buen día</p>" +
                "<p><b>OKISpain S.A.</b></p>";

            try
            {
                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje);
                correo.IsBodyHtml = true;
                correo.Attachments.Add(new Attachment(obtenerStreamFile(rutaFichero), Path.GetFileName(rutaFichero), MediaTypeNames.Text.Plain));
                //correo.Attachments.Add(new Attachment(rutaFichero, MediaTypeNames.Text.Plain));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }


        //Nos realiza el envío del correo a los Talleres
        // Para enviarles 2 ficheros de texto
        // Tanto del material solicitado cómo el que falta
        internal static bool operacionEnvioFicheroOrdenPedido_Talleres(string rutaFicheroWorkcenter, string rutaFicheroPedidoCompra)
        {
            string remitente = "alumnos2damAlmunia@gmail.com";
            string miclave = "2dam2dam ";
            string destinatario = "llucbue379@iesalmunia.com";
            string asunto = "OKISpain [Información de su pedido]";
            string mensaje = "<h2>Desde Almacenes OKISpain</h2>\n" +
                "<p>Les informamos acerca de su pedido</p>" +
                "<hr/>" +
                "<p>Aquí les dejamos todos los datos y la documentación pertinente</p>" +
                "<p>Les rogamos que sean pacientes</p>" +
                "\n<br/>" +
                "<p>Pasen un buen día y gracias por confiar en nosotros</p>" +
                "<p><b>OKISpain S.A.</b></p>";

            try
            {
                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje);
                correo.IsBodyHtml = true;

                //correo.Attachments.Add(new Attachment(rutaFicheroWorkcenter, MediaTypeNames.Text.Plain));
                correo.Attachments.Add(new Attachment(obtenerStreamFile(rutaFicheroWorkcenter), Path.GetFileName(rutaFicheroWorkcenter), MediaTypeNames.Text.Plain));

                if (rutaFicheroPedidoCompra != null)
                    correo.Attachments.Add(new Attachment(obtenerStreamFile(rutaFicheroPedidoCompra), Path.GetFileName(rutaFicheroPedidoCompra), MediaTypeNames.Text.Plain));
                //correo.Attachments.Add(new Attachment(rutaFicheroPedidoCompra, MediaTypeNames.Text.Plain));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        // Nos realiza el envío de correo informando acerca de
        // la información de los pedidos enviados y los pedidos con errores
        internal static bool operacionEnvioFicheroProceso(string rutaFichero)
        {
            string remitente = "alumnos2damAlmunia@gmail.com";
            string miclave = "2dam2dam ";
            string destinatario = "llucbue379@iesalmunia.com";
            string asunto = "Información rutinaria acerca de los pedidos";
            string mensaje = "<h2>Desde Almacenes OKISpain</h2>\n" +
                "<p>Les informamos acerca de uno de nuestros pedidos recibidos</p>" +
                "<hr/>" +
                "<p>Aquí les dejamos todos los datos y la documentación pertinente</p>" +
                "\n<br/>" +
                "<p>Pasen un buen día</p>" +
                "<p><b>OKISpain S.A.</b></p>";

            try
            {

                MailMessage correo = new MailMessage(remitente, destinatario, asunto, mensaje);
                correo.IsBodyHtml = true;
                //correo.Attachments.Add(new Attachment(rutaFichero, MediaTypeNames.Text.Plain));
                correo.Attachments.Add(new Attachment(obtenerStreamFile(rutaFichero), Path.GetFileName(rutaFichero), MediaTypeNames.Text.Plain));

                SmtpClient cliente = new SmtpClient("smtp.gmail.com");
                cliente.Credentials = new NetworkCredential(remitente, miclave);
                cliente.Port = 587;
                cliente.EnableSsl = true;

                cliente.Send(correo);

            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        //Método que te devuelve el stream de un fichero, para que no se bloquee el fichero al hacer el adjuntar en el correo y poder luego borrar esos ficheros temporales
        private static Stream obtenerStreamFile(string filePath)
        {
            using (FileStream fileStream = File.OpenRead(filePath))
            {
                MemoryStream memStream = new MemoryStream();
                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);

                return memStream;
            }
        }
    }
}
