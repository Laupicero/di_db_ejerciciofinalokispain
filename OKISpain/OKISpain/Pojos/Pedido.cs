﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OKISpain
{

    /// <summary>
    /// Clase creada para poder representar mejor todos los pedidos que tengamos
    /// Con todos sus productos, características y ficheros para envíar
    /// </summary>
    class Pedido
    {
        // Listado de articulos
        private List<Articulo> articulosDelPedido;

        //Datos pedido
        private String numPedido_TicketID;
        private String codigo_Warranty_Articlenumber;
        private String descrip_Warranty_ArticleDescription;
        private String serie_Warranty_Serialnumber;
        private String workCenter;
        private String cp_Cliente_Loc_Post_code;
        private String poblacionCliente_LocCity;
        private String nomb_Cliente_Submitter;
        private String dir_Cliente_Loc_Street;
        private DateTime fecha_XML_Date;

        // ficheros .txt
        private String rutaFicheroWorkCenter;
        private String rutaFicheroPedidoCompra;
        private String rutaFicheroProceso;

        // Constructor
        public Pedido() { }

        // MODIFICADORES DE ACCESO
        public string NumPedido_TicketID { get => numPedido_TicketID; set => numPedido_TicketID = value; }
        public string Codigo_Warranty_Articlenumber { get => codigo_Warranty_Articlenumber; set => codigo_Warranty_Articlenumber = value; }
        public string Descrip_Warranty_ArticleDescription { get => descrip_Warranty_ArticleDescription; set => descrip_Warranty_ArticleDescription = value; }
        public string WorkCenter { get => workCenter; set => workCenter = value; }
        public string CP_Cliente_Loc_Post_code1 { get => cp_Cliente_Loc_Post_code; set => cp_Cliente_Loc_Post_code = value; }
        public string PoblacionCliente_LocCity { get => poblacionCliente_LocCity; set => poblacionCliente_LocCity = value; }
        public string Nomb_Cliente_Submitter { get => nomb_Cliente_Submitter; set => nomb_Cliente_Submitter = value; }
        public string Dir_Cliente_Loc_Street { get => dir_Cliente_Loc_Street; set => dir_Cliente_Loc_Street = value; }
        public DateTime Fecha_XML_Date { get => fecha_XML_Date; set => fecha_XML_Date = value; }
        public string RutaFicheroWorkCenter { get => rutaFicheroWorkCenter; set => rutaFicheroWorkCenter = value; }
        public string RutaFicheroPedidoCompra { get => rutaFicheroPedidoCompra; set => rutaFicheroPedidoCompra = value; }
        public string RutaFicheroProceso { get => rutaFicheroProceso; set => rutaFicheroProceso = value; }
        public string Serie_Warranty_Serialnumber { get => serie_Warranty_Serialnumber; set => serie_Warranty_Serialnumber = value; }
        internal List<Articulo> ArticulosDelPedido { get => articulosDelPedido; set => articulosDelPedido = value; }

       
    }
}
