﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OKISpain
{
    /// <summary>
    /// Clase creada para poder representar mejor todos los artículos que tengamos en nuestro pedido
    /// </summary>
    class Articulo
    {
        private string codPedido;
        private string descrip;
        private string almacen;
        private string ubicacion;

        //Cosntructores
        public Articulo() { }
        public Articulo(string codPedido, string descrip, string almacen, string ubicacion)
        {
            this.codPedido = codPedido;
            this.descrip = descrip;
            this.almacen = almacen;
            this.ubicacion = ubicacion;
        }


        // MODIFICADORES DE ACCESO
        public string CodPedido { get => codPedido; set => codPedido = value; }
        public string Descrip { get => descrip; set => descrip = value; }
        public string Almacen { get => almacen; set => almacen = value; }
        public string Ubicacion { get => ubicacion; set => ubicacion = value; }
    }
}
